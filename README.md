# doe_dash

The following project requires a **URL/path structure** in order to function.

The structure required is

*"\content\doe\sws\schools\l\liverpool-h"*

where *"l\liverpool-h"* may be replaced with other school pathways (e.g. *'c\castlehill-h'* or *"e\endaginew-p"*, etc)

If testing locally, this can be done by recreating the directory structure (e.g. *"C:\Users\example\content\doe\sws\schools\l\liverpool-h"*).
