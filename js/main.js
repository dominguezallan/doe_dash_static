// Constants set up
const MONTH_SIZE = 30;
const WEEK_SIZE = 7;
const COLORS = ['#F1948A', '#AF7AC5', '#2980B9', '#17A589', '#F7DC6F', '#F39C12', '#E59866', '#C0392B', '#7D3C98', '#5DADE2', '#2ECC71', '#E67E22'];
const GREEN_TEN_SHADES = ['#145A32', '#145A32', '#196F3D', '#196F3D', '#1E8449', '#1E8449', '#229954', '#229954', '#27AE60', '#27AE60'];

// Date setup
var today = new Date();
var todayString = today.toISOString().split('T')[0];
var monthAgo = new Date();
monthAgo.setDate(today.getDate() - MONTH_SIZE);
var monthAgoString  = monthAgo.toISOString().split('T')[0];
var weekAgo = new Date();
weekAgo.setDate(today.getDate() - WEEK_SIZE);
var weekAgoString = weekAgo.toISOString().split('T')[0];
var startDate;
var endDate;
var initial = true;
var dataCount = 0;

// Homepage URL - to isolate schools
var thisUrl = window.location.href;
var regexp = /(.*)(\/content\/doe\/sws\/schools\/.\/)([a-z-]*)(.*)/;
var regResult = thisUrl.match(regexp)[3];
const SCHOOL = regResult;
console.log(SCHOOL);
const SCHOOL_URL = "https://" + SCHOOL + ".schools.nsw.gov.au/";
console.log(SCHOOL_URL);

var viewsGraph = new lineGraph("#pageviews-canvas", { name : "Page Views", pointLabel : "Page Views"});

var newRetDonut = new donutGraph("#newold-canvas", { name : "New vs Return Visitors" });

var browserDonut = new donutGraph("#browser-canvas", { name : "Browser Type" });

var mobileDonut = new donutGraph("#mobile-canvas", { name : "Mobile Device Type" });

var osHbar = new hbarChart("#os-canvas", { name : "Operating System Used", label : "Users" });

var topHbar = new hbarChart("#top-canvas", { name : "Top pages viewed", label : "Views" });

var refDonut = new donutGraph("#referrer-canvas", { name : "Site Visited From" });

// var donutChart = new DonutChart("#newold-donut", { width: 300, height: 450 });

// store reports for use
var reportArr = {};

// dictionary of report type to report id
var reportDict = {};

// code to run when the HTML is fully loaded
$( document ).ready(function() {
  initDate();
  sendRequests(monthAgoString, todayString);
  changeDate();
});

// event triggered when Report ID received; calls checkQueue function
$( document ).on("report-id-received", function(event, id) {
  // console.log("checkQueue start: " + id);
  checkQueue(id);
});

// function to trigger event
function clearEvent (id) {
  let event = jQuery.Event("report-queue-cleared");
  // console.log(response.responseJSON.reportID);
  $( document ).trigger(event, id);
}

// event triggered when report queue is clear (of report ID)
$( document ).on("report-queue-cleared", function(event, id) {
  // console.log("Report is ready");
  makeGetRequest(id);
});
