/**
* Function to pass multiple report requests.
* Currently not using a loop to avoid race conditions.
* Therefore, manually separating calls by X milliseconds.
*/

var callQueue = 'Report.Queue';

function sendRequests (start, end) {
  var queuePage = "pages";    // to identify type of report
  var paramPage = {
    "reportDescription":{
      "reportType":"overtime",
      "reportSuiteID":config.reportSuite,
      "dateFrom":start,
      "dateTo":end,
      "dateGranularity":"day",
      "elements":[
        {
          "id" : "prop2",
          "selected" : [SCHOOL_URL]
        }
      ],
      "metrics":[
        {
          "id" : "pageviews"
        }
      ]
    }
  }
  try {
    makeQueueRequest(callQueue, paramPage, queuePage);    // page views
  } catch (e) {
    console.log(e);
  }
  var queueVisits = "visits";    // to identify type of report

  // overtime data call
  var paramVisits = {
    "reportDescription":{
      "reportType":"overtime",
      "reportSuiteID":config.reportSuite,
      "dateFrom":start,
      "dateTo":end,
      "elements":[
        {
          "id" : "prop2",
          "selected" : [SCHOOL_URL]
        }
      ],
      "metrics":[
        {
          "id" : "visits"
        },
        {
          "id" : "visitors"
        },
        {
          "id" : "pageviews"
        }
      ]
    }
  }

  var queueBrow = "browsertype";
  var paramBrow = {
    "reportDescription":{
      "reportType":"overtime",
      "reportSuiteID":config.reportSuite,
      "dateFrom":start,
      "dateTo":end,
      "elements":[
        {
          "id" : "prop2",
          "selected" : [SCHOOL_URL]
        },
        {
          "id" : "browsertype"
        }
      ],
      "metrics":[
        {
          "id" : "visits"
        }
      ]
    }
  }

  var queueNewRet = "newreturn";
  var paramNewRet = {
    "reportDescription":{
      "reportType":"overtime",
      "reportSuiteID":config.reportSuite,
      "dateFrom":start,
      "dateTo":end,
      "elements":[
        {
          "id" : "prop2",
          "selected" : [SCHOOL_URL]
        },
        {
          "id" : "evar6"
        }
      ],
      "metrics":[
        {
          "id" : "visits"
        }
      ]
    }
  }

  var queueMob = "mobiletype";
  var paramMob = {
    "reportDescription":{
      "reportType":"overtime",
      "reportSuiteID":config.reportSuite,
      "dateFrom":start,
      "dateTo":end,
      "elements":[
        {
          "id" : "prop2",
          "selected" : [SCHOOL_URL]
        },
        {
          "id" : "mobiledevicetype"
        }
      ],
      "metrics":[
        {
          "id" : "visits"
        }
      ]
    }
  }

  var queueOS = "ostype";
  var paramOS = {
    "reportDescription":{
      "reportType":"overtime",
      "reportSuiteID":config.reportSuite,
      "dateFrom":start,
      "dateTo":end,
      "elements":[
        {
          "id" : "prop2",
          "selected" : [SCHOOL_URL]
        },
        {
          "id" : "operatingsystemtype"
        }
      ],
      "metrics":[
        {
          "id" : "visits"
        }
      ]
    }
  }

  var queueTop = "toppage";
  var paramTop = {
    "reportDescription": {
      "reportSuiteID": config.reportSuite,
      "dateFrom":start,
      "dateTo":end,
      "metrics": [
        { "id": "pageviews" }
      ],
      "elements": [
        { "id": "prop1", "top" : "10" }
      ],
      "segments": [
        {
          "container":{
            "type":"hits",
            "rules":[
              {
                "name":"Top Pages",
                "element":"prop2",
                "operator":"contains",
                "value":SCHOOL_URL
              }
            ]
          }
        }
      ],
    }
  }

  var queueName = "schoolname";
  var paramName = {
    "reportDescription":{
      "reportSuiteID":config.reportSuite,
      "elements":[
        {
          "id" : "prop2",
          "selected" : [SCHOOL_URL]
        },
        {
          "id" : "evar11"
        }
      ]
    }
  }

  var queueRef = "referrertype";
  var paramRef = {
    "reportDescription":{
      "reportType":"overtime",
      "reportSuiteID":config.reportSuite,
      "dateFrom":start,
      "dateTo":end,
      "elements":[
        {
          "id" : "prop2",
          "selected" : [SCHOOL_URL]
        },
        {
          "id" : "referrertype"
        }
      ],
      "metrics":[
        {
          "id" : "visits"
        }
      ]
    }
  }

  try {
    makeQueueRequest(callQueue, paramName, queueName);    // school name
    makeQueueRequest(callQueue, paramVisits, queueVisits);  // visits data
    makeQueueRequest(callQueue, paramBrow, queueBrow);  // browser type
    makeQueueRequest(callQueue, paramNewRet, queueNewRet);     // new v/s return
    makeQueueRequest(callQueue, paramMob, queueMob);  // mobile type
    makeQueueRequest(callQueue, paramOS, queueOS);  // OS type
    makeQueueRequest(callQueue, paramTop, queueTop);  // top pages
    makeQueueRequest(callQueue, paramRef, queueRef);  // referrer type
  } catch (err) {
    console.log(err);
  }
}

function makeOverviewReq (start, end) {

}

/**
* 1. Make Queue request to make report.
* 2. Wait for Queue response to be received (id), THEN trigger an event and send response in json
*/
function makeQueueRequest(call, param, type) {
  MarketingCloud.makeRequest(config.username, config.secret, call, param, config.endpoint, function(response){
    let event = jQuery.Event("report-id-received");
    // console.log(response.responseJSON.reportID);
    reportDict[response.responseJSON.reportID] = type;
    $( document ).trigger(event, response.responseJSON.reportID);
  });
  dataCount++;
};

/**
* 3. Make interval calls (5000ms) to get queue
*    4. From each queue list (response), check for id
*        5. If id is in list, leave interval
*        6. If not in list, stop interval and trigger event
*/
function checkQueue (id) {
  let callGetQueue = 'Report.GetQueue';
  let paramGetQueue = [];
  let count = 0;
  (function requestQueue () {
    count++;
    MarketingCloud.makeRequest(config.username, config.secret, callGetQueue, paramGetQueue, config.endpoint, function(response) {
      let arr = response.responseJSON;
      // console.log("arr list: ", arr);
      if (arr.length > 0) {
        let boo = false;
        for (let i=0; i<arr.length; i++) {
          if (arr[i].reportID == id) {
            setTimeout(requestQueue, 1000);
            break;
          }
          if (i == arr.length-1) {
            // console.log("Loop cleared");
            clearEvent(id);
          }
        }

      }
      else {
        // console.log("Empty cleared");
        clearEvent(id);
      }
    });
  }());
}

/**
* 7. Make request to retrieve the data, and display data
*/
function makeGetRequest (id) {
  var callGet = 'Report.Get';
  var paramGet = {
    "reportID": id
  }
  // make the API call
  MarketingCloud.makeRequest(config.username, config.secret, callGet, paramGet, config.endpoint, function(response) {

    // // JSON Display of Data
    // $('#dashboard').append('<p>API Response (' + id + '): <pre>' + JSON.stringify(response.responseJSON, undefined, 4) + '</pre></p>');
    // store JSON data
    reportArr[id] = response.responseJSON.report;

    let event = jQuery.Event("report-retrieved");
    // console.log(response.responseJSON.reportID);
    $( document ).trigger(event, id);
  });
}

// event triggered when report is received
$( document ).on("report-retrieved", function(event, id) {
  // console.log(reportArr[id]);
  let rEvent;
  switch(reportDict[id]) {
    case "pages":
    rEvent = jQuery.Event("page-data-received");
    break;
    case "visits":
    rEvent = jQuery.Event("visits-data-received");
    break;
    case "browsertype":
    rEvent = jQuery.Event("browser-data-received");
    break;
    case "mobiletype":
    rEvent = jQuery.Event("mobile-data-received");
    break;
    case "ostype":
    rEvent = jQuery.Event("os-data-received");
    break;
    case "newreturn":
    rEvent = jQuery.Event("newret-data-received");
    break;
    case "toppage":
    rEvent = jQuery.Event("top-data-received");
    break;
    case "schoolname":
    rEvent = jQuery.Event("name-data-received");
    break;
    case "referrertype":
    rEvent = jQuery.Event("referrer-data-received");
    break;
    default:
    rEvent = jQuery.Event("error", "undefined");
  }
  $( document ).trigger(rEvent, reportArr[id]);
});
