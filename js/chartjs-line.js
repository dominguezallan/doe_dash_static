
function lineGraph (id, config) {

  let chart;

  let type = "line";

  let labels = [];
  let dataSets = {
    label : config.pointLabel,
    data : [],
    backgroundColor : "#CB4335",
    borderColor : "#EC7063",
    fill : false,
    lineTension : 0,
    pointStyle : 'rect',
    pointRadius : 5
  }

  let options = {
    maintainAspectRatio : false,
    responsive: true,
    title: {
      display: true,
      position: "top",
      text: config.name,
      fontSize: 18,
      fontColor: "#111"
    },
    scales: {
      xAxes: [{
        type: 'time',
        time: {
          unit: 'day',
          unitStepSize: 1,
          displayFormats: {
           'day': 'MMM DD'
          }
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          callback: function(value, index, values) {
            if (Math.floor(value) === value) {
              return value;
            }
          }
        }
      }]
    }
  }

  let dataObj = {
    labels : labels,
    datasets : [
      dataSets
    ]
  }

  this.addData = function (data) {
    for (var i = 0; i < data.length; i++) {
      labels.push(
        moment(data[i].date)
      );
  		dataSets.data.push({
  			t: moment(data[i].date),
  			y: data[i].units
  		});
  	}
    dataObj.datasets = [dataSets];
  }

  this.removeData = function () {
    labels.length = 0;
    dataSets.data.length = 0;
    dataObj.datasets = [dataSets];
    chart.update();
  }

  this.drawGraph = function () {
    if (chart == null) {
      let ctx = $(id);
      chart = new Chart (ctx, {
        type : type,
        data : dataObj,
        options : options
      });
    } else {
      chart.update();
    }
  }
}
