
function hbarChart (id, config) {

  let chart;

  let type = "horizontalBar";

  let labels = [];
  let dataSets = {
    label : config.label,
    backgroundColor : [],
    data : []
  };

  let options = {
    maintainAspectRatio : false,
    title: {
      display: true,
      text: config.name,
      fontSize: 18,
      fontColor: "#111"
    },
    scales: {
      xAxes: [{
        ticks: {
          min: 0,
          callback: function(value, index, values) {
            if (Math.floor(value) === value) {
              return value;
            }
          }
        }
      }]
    },
    legend: {
      display: false
    }
  };

  let dataObj = {
    labels : labels,
    datasets : [
      dataSets
    ]
  };

  this.addData = function (data) {
    for (var i = 0; i < data.length; i++) {
      labels.push(
        data[i].name
      );
  		dataSets.data.push(
        data[i].count
  		);
      dataSets.backgroundColor.push(
        data[i].color
      );
  	}
    dataObj.datasets = [dataSets];
  }

  this.removeData = function () {
    labels.length = 0;
    dataSets.data.length = 0;
    dataObj.datasets = [dataSets];
    chart.update();
  }

  this.drawGraph = function () {
    if (chart == null) {
      let ctx = $(id);
      chart = new Chart (ctx, {
        type : type,
        data : dataObj,
        options : options
      });
    } else {
      chart.update();
    }
  }

}
