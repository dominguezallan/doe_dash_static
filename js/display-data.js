// ******** Page Data display *********

/**
* Display data in charts
* - Page Views trend graph. Page views over given time frame (per day)
* - Page Views big number. Current (past 24hrs) page views
*/
// trends graph
$( document ).on("page-data-received", function(event, report) {
  // pull the minute totals for each minute
  // formatted data is [100, 200, 300, ...] (newest data last)
  // console.log("page: ", report);
  let data = [];
  let day;
  for (let i=0; i<report['data'].length; i++) {
    day = report.data[i];
    data.push({
      'date' : new Date(day.year, day.month-1, day.day),
      'units' :  parseInt(day.breakdown[0].counts[0])
    });
  }

  viewsGraph.addData(data);
  viewsGraph.drawGraph();

  dataCheck();

});


// ******* Operating System Data Display (Number) *******
$( document ).on("referrer-data-received", function(event, report) {
  // grab the total for this time period
  // console.log("referrer: ", report);
  let data = [];
  let breakdown = report.data[0].breakdown;
  for (let i=0; i<breakdown.length; i++) {
    data.push({
      'name' : breakdown[i].name,
      'count' : parseInt(breakdown[i].counts[0]),
      'color' : COLORS[i]
    });
  };

  refDonut.addData(data);
  refDonut.drawGraph();

  dataCheck();
});

// number counter
$( document ).on("visits-data-received", function(event, report) {
  // grab the total for this time period
  // console.log("visits: ", report);
  let reportSize = report["data"].length;
  let visits = report.data[0].counts[0];
  let visitors = report.data[0].counts[1];
  let pageviews = report.data[0].counts[2];

  // add a comma every thousand numbers (i.e. 1000 => 1,000)
  let commaStep = $.animateNumber.numberStepFactories.separator(',');

  //Animate the number
  $('#visits-current').animateNumber({
    number:visits,
    numberStep: commaStep
  }, 500);
  $('#visitors-current').animateNumber({
    number:visitors,
    numberStep: commaStep
  }, 500);
  $('#pageviews-current').animateNumber({
    number:pageviews,
    numberStep: commaStep
  }, 500);

  dataCheck();
});

// ******* New vs Return Data Display (Number) *******
$( document ).on("newret-data-received", function(event, report) {
  // grab the total for this time period
  // console.log("newret: ", report);
  let data = [];
  let breakdown = report.data[0].breakdown;
  let newPerc = 0;
  let total = 0;
  for (let i=0; i<breakdown.length; i++) {
    data.push({
      'name' : breakdown[i].name,
      'count' : parseInt(breakdown[i].counts[0]),
      'color' : COLORS[i]
    });
    total += parseInt(breakdown[i].counts[0]);
    if (breakdown[i].name == "New") {
      newPerc = parseInt(breakdown[i].counts[0]);
    }
  };

  newRetDonut.addData(data);
  newRetDonut.drawGraph();

  newPerc = Math.round((newPerc / total) * 100);
  var percent_number_step = $.animateNumber.numberStepFactories.append('%');  // appendss '%' symbol

  $('#new-perc').animateNumber({
    number:newPerc,
    numberStep: percent_number_step
  }, 500);

  dataCheck();

});

// ******* Browser Data Display (Number) *******
$( document ).on("browser-data-received", function(event, report) {
  // grab the total for this time period
  // console.log("browser: ", report);
  let data = [];
  let breakdown = report.data[0].breakdown;
  for (let i=0; i<breakdown.length; i++) {
    data.push({
      'name' : breakdown[i].name,
      'count' : parseInt(breakdown[i].counts[0]),
      'color' : COLORS[i]
    });
  };

  browserDonut.addData(data);
  browserDonut.drawGraph();

  dataCheck();
});

// ******* Mobile Device Data Display (Number) *******
$( document ).on("mobile-data-received", function(event, report) {
  // grab the total for this time period
  // console.log("mobile: ", report);
  let data = [];
  let breakdown = report.data[0].breakdown;
  for (let i=0; i<breakdown.length; i++) {
    data.push({
      'name' : breakdown[i].name,
      'count' : parseInt(breakdown[i].counts[0]),
      'color' : COLORS[i]
    });
  };

  mobileDonut.addData(data);
  mobileDonut.drawGraph();

  dataCheck();
});

// ******* Operating System Data Display (Number) *******
$( document ).on("os-data-received", function(event, report) {
  // grab the total for this time period
  // console.log("os: ", report);
  let data = [];
  let breakdown = report.data[0].breakdown;
  for (let i=0; i<breakdown.length; i++) {
    data.push({
      'name' : breakdown[i].name,
      'count' : parseInt(breakdown[i].counts[0]),
      'color' : COLORS[i]
    });
  };

  osHbar.addData(data);
  osHbar.drawGraph();

  dataCheck();
});

// ******* Top Pages Data Display (Number) *******
$( document ).on("top-data-received", function(event, report) {
  // grab the total for this time period
  // console.log("Top (last): ", report);
  let data = [];
  let page;
  for (let i=0; i<report['data'].length; i++) {
    page = report.data[i];
    data.push({
      'name' : page.name,
      'count' : parseInt(page.counts[0]),
      'color' : GREEN_TEN_SHADES[i]
    });
  }

  topHbar.addData(data);
  topHbar.drawGraph();

  dataCheck();

});

// ******* Display School Name *******
$( document ).on("name-data-received", function(event, report) {
  // grab the total for this time period
  // console.log("name: ", titleCase(report.data[0].breakdown[0].name));

  $("#school-name").text("For " + titleCase(report.data[0].breakdown[0].name));
  function titleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  dataCheck();

});

// Check if all data is loaded
function dataCheck () {
    dataCount--;
    //console.log(dataCount);
    if (dataCount == 0) {
        // hide loading screen
        document.getElementById("start-load").style.display = "none";
    }
}
