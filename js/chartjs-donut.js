
function donutGraph (id, config) {

  let chart;

  let type = "doughnut";

  let labels = [];
  let dataSets = {
    backgroundColor : [],
    data : []
  }

  let options = {
    maintainAspectRatio : false,
    title: {
      display: true,
      text: config.name,
      fontSize: 18,
      fontColor: "#111"
    }
  }

  let dataObj = {
    labels : labels,
    datasets : [
      dataSets
    ]
  }

  this.addData = function (data) {
    for (var i = 0; i < data.length; i++) {
      labels.push(
        data[i].name
      );
  		dataSets.data.push(
        data[i].count
  		);
      dataSets.backgroundColor.push(
        data[i].color
      );
  	}
    dataObj.datasets = [dataSets];
  }

  this.removeData = function () {
    labels.length = 0;
    dataSets.data.length = 0;
    dataObj.datasets = [dataSets];
    chart.update();
  }

  this.drawGraph = function (data) {
    if (chart == null) {
      let ctx = $(id);
      chart = new Chart (ctx, {
        type : type,
        data : dataObj,
        options : options
      });
    } else {
      chart.update();
    }
  }

}
